package com.waiget.chessgame.Database;

import com.waiget.chessgame.Entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by Wai Get on 18/06/2017.
 */
@Service
public class DataLoader {

    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    private void initDatabase() {

        // To make admin from start

        userRepository.deleteAll();
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        User user = new User();
        user.setUsername("waiget");
        user.setPassword(passwordEncoder.encode("secret"));
        user.addRole("ROLE_USER");
        user.addRole("ROLE_ADMIN");

        userRepository.save(user);

    }
}

package com.waiget.chessgame.Database;

import com.waiget.chessgame.Entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Wai Get on 18/06/2017.
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {
    public List<User> findAll();
    public User findOne(String id);
    public User findByUsername(String username);
    public User save(User user);
    public void delete(User user);
}

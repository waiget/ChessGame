package com.waiget.chessgame.Controllers;

import com.waiget.chessgame.Database.UserRepository;
import com.waiget.chessgame.Entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by Wai Get on 18/06/2017.
 */
@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(path = "/api/newuser", method=RequestMethod.POST)
    public User createUser(@RequestBody User user) {
        user.addRole("ROLE_USER");
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @RequestMapping(path = "/api/getcurrentuser", method=RequestMethod.GET)
    @ResponseBody
    public User currentUser(Principal principal) {
        return userRepository.findByUsername(principal.getName());
    }
}

(function(){

  var app = angular.module("Chess", ["ngRoute"]);

  app.config(function($routeProvider){
    $routeProvider
      .when("/main", {
        templateUrl: "main.html",
        controller: "MainController"
      })
      .otherwise({redirectTo:"/main"});
  });

}());
